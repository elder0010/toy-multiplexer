init_scene:
		
		lda #BORDER_COLOUR
		sta $d020

		lda #BG_COLOUR
		sta $d021

		//Disegniamo i tiles del livello
		jsr clear_screen
		jsr draw_tiles
		jsr draw_scores_panel
		jsr color_tiles

		//prepariamo gli sprites
		jsr init_sprites

		//tutto fatto? ok possiamo switchare su bank 1
		lda $dd00
		and #%11111100
		ora #%00000010
		sta $dd00

		//e impostare $d018 altrimenti non vediamo nulla!
		lda #d018_game_screen
		sta $d018

		rts

clear_screen:
		ldx #0
		lda #0
!:
		sta screen,x
		sta screen+$100,x
		sta screen+$200,x
		sta screen+$300,x
		dex
		bne !-

		rts

color_tiles:
		ldx #0
		lda #TILES_COLOUR
!:
		sta $d800,x
		sta $d800+$100,x
		sta $d800+$200,x
		sta $d800+$300,x
		dex
		bne !-

		rts

draw_scores_panel:
		lda #2
		ldx #$27
!:
		sta screen+$28*22,x
		sta screen+$28*23,x
		sta screen+$28*24,x
		dex
		bpl !-

		rts

draw_tiles:
		
		lda #3       //devo disegnare 4 piattaforme
		sta platform //perchè metto 3 e non 4 nell'indice?
                     //perchè il loop sotto termina con una bpl e NON bne
                     //vedi fine della funzione qui sotto

another_platform:
		//uso la y della piattaforma corrente
		//per recuperare l'offset sulla memoria schermo
		//su y ovvero la riga
		ldx platform

		lda platforms_y,x
		tay

		lda screen_rows_lo,y
		sta target+1

		lda screen_rows_hi,y
		sta target+2

		//recupero la x della piattaforma corrente
		//questo è un "displacement" rispetto a y
		lda platforms_x_end,x //<- occhio in x cè ancora il valore di platform (vedi sopra)
		sta curlen+1 //modifico il valore nel loop sotto (target/curlen)

		lda platforms_x_start,x
		tax

		lda #1 		//tile "pieno"
!:
target:
		sta $ffff,x //self modifying code: qui non ci sarà $ffff, ma l'indirizzo
		inx			//recuperato prima da screen_rows_lo e screen_rows_hi
curlen:
		cpx #00     //anche qui! il valore è stato modificato da sta curlen+1 (vedi sopra)
		bne !-

		dec platform
		bpl another_platform //fatte tutte le piattaforme? la condizione di salto qui è BPL
							 //branch on PLUS, quindi il loop terminerà quando platform varrà
							 //$ff. Facendo i conti da 3 si ha: $3,$2,$1,$0,$ff.
							 //4 piattaforme e fine loop quindi. 

							 //l'alternativa sarebbe stata fare lda #0 sta platform e far terminare
							 //il loop con inc platform cmp #4 bne another_platform ma sarebbe stato
							 //piu' lento! occhio: BNE another_platform non BPL another_platform
		rts

init_sprites:
		
		lda #0
		sta $d017 //niente stretch su Y su tutti gli sprites
		sta $d01d //niente stretch su X su tutti gli sprites
		sta $d01c //niente multicolour su tutti gli sprites
		sta bonus_sprite_wob_pt

		lda #$ff  //abilito tutti gli sprites
		sta $d015

		//Faccio puntare gli sprite dallo schermo..
		lda #[sprites/$40]
		ldx #08
!:
		sta screen+$03f8,x
		dex
		bpl !-


		//lo sprite 0 lo appoggiamo in basso a sx
		lda #30
		sta $d000

		lda #$c5
		sta $d001

		rts

/*
Routine di riposizionamento degli sprite:
viene chiamata all'inizio di ogni IRQ di plex
gli sprite vengono riposizionati su Y in base
alle "future" coordinate (quelle della prox piattaforma)

e su X in base alla tabella che abbiamo calcolato nell'irq
inferiore.
Nota come il lo byte di X è settato per ogni sprite
mentre l'hi byte di X è settato con un colpo solo per tutti (con $d010)
*/
reposition_sprite:
		ldx platform

		//la Y è in comune
		lda sprite_y,x
		sta $d003
		sta $d005
		sta $d007
		sta $d009
		sta $d00b
		sta $d00d

		//hi byte settato in un colpo solo per tutti gli sprite con $d010
		lda sprite_x_hi,x
		sta $d010

		//lo byte settato per ogni sprite
		ldx global_sprite_counter
		lda sprite_x_lo,x
		sta $d002
		lda sprite_x_lo+1,x
		sta $d004
		lda sprite_x_lo+2,x
		sta $d006
		lda sprite_x_lo+3,x
		sta $d008
		lda sprite_x_lo+4,x
		sta $d00a
		lda sprite_x_lo+5,x
		sta $d00c

		clc
		txa
		adc #6
		sta global_sprite_counter //la prox volta lavorerò sui prox 6 sprite..

		rts


//-------------------------------------------------------
/*
Ogni piattaforma gestisce 6 sprites "nemici"
Per ogni sprite considero la direzione: avanti o indietro.

Ricorda che la posizione dello sprite su X è data (purtroppo)
da un valore a 9 bit.

I bit 0-7 della x degli sprite vengono memorizzati in
$d000 - sprite 0
$d002 - sprite 1
$d004 - sprite 2

e cosi via fino a $d000e

il bit 8 viene gestito (per tutti gli 8 sprite)
da $d010

in pratica se $d010 vale ad es #3 (che in binario è #%00000011)
gli sprite 0 e 1 hanno la coordinata su x data $d000 + 256 e $d002 + 256

La routine funziona cosi:

PER OGNI PIATTAFORMA
	PER OGNI SPRITE DELLA PIATTAFORMA (cioè 6 :D)
		se direzione = AVANTI E
		se l'hi byte (x) è uguale all'hi byte del bound DX della piattaforma E 
		se il lo byte (x) è < all'lo byte del bound DX della piattaforma E
		 -> allora è arrivato il momento di cambiare direzione!
		altrimenti posso incrementare di 1 la posizione dello sprite

		se direzione = INDIETRO E
		se l'hi byte (x) è uguale all'hi byte del bound SX della piattaforma E 
		se il lo byte (x) è >= all'lo byte del bound SX della piattaforma E
		 -> allora è arrivato il momento di cambiare direzione!
		altrimenti posso decrementare di 1 la posizione dello sprite

Le direzioni e le coordinate X degli sprite vengono memorizzati in due tabelle

sprite_x_lo
sprite_x_hi

i valori di $d010 per ogni piattaforma finiranno in sprite_x_hi

nota che siccome $d010 gestisce piu' sprite contemporaneamente, il valore di $d010
viene "costruito" loopando sui 6 sprite e accendendo/spegnendo con un ORA/AND il relativo
bit dello sprite.

Nota come le routine AVANTI e INDIETRO sono assolutamente identiche come logica, cambiano
solo i bound per il confronto (limite sx o dx) e il bit $d010 da accendere O spegnere.
Lo so è un po complesso :D

Gli sprite che non vuoi visualizzare li metti semplicemente con coordinata x a 0 e finiscono a sx
coperti dal bordo.
*/

move_enemies:
		lda #1
		sta sprite_cycler //questo loopa 1-7 sui 6 sprites

		ldx #0
		stx line_ct

		lda sprite_x_hi
		sta tmp

		lda #6
		sta spr_limit+1

//---------------------------------------------
//START LOOP SUI 6 SPRITES
newsprite:
		lda sprite_dir,x
		bne indietro
avanti:
		ldy sprite_cycler
		lda tmp //ha 1 dove lo sprite sta a dx
		and d010_mask,y
		beq !+
		lda #1
		jmp cmpr
!:
		lda #0
cmpr:
		ldy line_ct
		cmp platforms_bound_right_hi,y
		bne !+

		lda sprite_x_lo,x
		cmp platforms_bound_right_lo,y
		bcc !+

		lda #1
		sta sprite_dir,x
!:
		lda sprite_x_lo,x
		clc
		adc #1
		sta sprite_x_lo,x
		bcc !+

		ldy sprite_cycler
		lda tmp
		ora d010_mask,y
		sta tmp
!:
		jmp end
indietro:
		ldy sprite_cycler
		lda tmp //ha 1 dove lo sprite sta a dx
		and d010_mask,y
		beq !+

		lda #1
		jmp cmpr_back
!:
		lda #0
cmpr_back:
		ldy line_ct
		cmp platforms_bound_left_hi,y
		bne !+

		lda sprite_x_lo,x
		cmp platforms_bound_left_lo,y
		bcs !+

		lda #0
		sta sprite_dir,x
!:

		lda sprite_x_lo,x
		sec
		sbc #1
		sta sprite_x_lo,x
		bcs end

		ldy sprite_cycler
		lda tmp
		and d010_mask_neg,y
		sta tmp
end:
		inc sprite_cycler
		lda sprite_cycler
		cmp #8
		bne !+
		lda #0
		sta sprite_cycler
!:
		inx
spr_limit:
		cpx #6 //#6
		beq !+
		jmp newsprite
//END LOOP SUI 6 SPRITES
//-------------------------------------------------------

//Prox piattaforma..
!:		
		lda tmp		
		ldy line_ct
		sta sprite_x_hi,y
		
		inc line_ct
		ldy line_ct
		cpy #plt_y.size()-1 //finito le piattaforme?
		beq !+

		//Resetto tutte le variabili e riparto con una nuova piattaforma!
		lda spr_limit+1
		clc 
		adc #6
		sta spr_limit+1

		lda #1
		sta sprite_cycler

		lda sprite_x_hi,y
		sta tmp

		jmp newsprite
!:
//Fine loop piattaforme
		
		//Nascondiamo gli sprite che non vogliamo mostrare
		//ad es nella piattaforma centrale ne vogliamo 
		//far vedere solo 3
		lda #0
		sta sprite_x_lo+6
		sta sprite_x_lo+7
		sta sprite_x_lo+8

		lda sprite_x_hi+1
		and #%11111000
		sta sprite_x_hi+1

		//Muovo lo sprite "bonus"
		ldx bonus_sprite_wob_pt
		lda bonus_wob_x,x
		sta $d00e

		lda bonus_wob_y,x
		sta $d00f

		inc bonus_sprite_wob_pt
		
		rts

.align 16
d010_mask:
.byte %00000001
.byte %00000010
.byte %00000100
.byte %00001000
.byte %00010000
.byte %00100000
.byte %01000000
.byte %10000000

d010_mask_neg:
.byte %11111110
.byte %11111101
.byte %11111011
.byte %11110111
.byte %11101111
.byte %11011111
.byte %10111111
.byte %01111111