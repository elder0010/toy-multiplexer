.var DEBUG_IRQ = false
.var DEBUG_MUSIC = false
.var DEBUG_MOVEMENT = false

.const BORDER_COLOUR = BLUE
.const BG_COLOUR = BLACK
.const TILES_COLOUR = BROWN

/*
Schema della memoria:
$2000 - mappa delle piattaforme e tabelle di utilità sprites
$4000 / $47ff - charset
$4800 / $4bff - screen memory
$4c40 in poi - sprites
*/
.const platform_map = $2000
.const bonus_sprite_wob_tables = $2c00
.const charset = $4000
.const screen = $4800
.const sprites = $4c40


//Mi calcolo il valore di $d018 per puntare lo schermo e il charset
//uso questa formuletta invece di un valore scritto "a mano" cosi
//se cambio i valori sopra di charset e screen non devo rifarmi il calcolo
.const d018_game_screen = [[[screen & $3fff] / $0400] << 4] + [[[charset & $3fff] / $0800] << 1]

//Varie zero page
.var platform = $20
.var global_sprite_counter = $21
.var sprite_n = $22
.var tmp = $23
.var line_ct = $24
.var sprite_cycler = $25
.var tmp2 = $26
.var bonus_sprite_wob_pt = $30
