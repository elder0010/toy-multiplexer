//WIZARD DEMO

//Fai riferimento a  http://sta.c64.org/cbm64mem.html

.import source "variables.asm"
.var music = LoadSid("data/knights.sid")
.pc=music.location "Music" .fill music.size, music.getData(i)


:BasicUpstart2(code) //macro di KickAssembler che ci fa saltare a $0900 (vedi label "code" qua sotto)

.pc = $0900 "start" //il programma inizia a $0900
code: 
		sei //fermo tutti gli irq (CIA, RASTER)

/*	Disattivo il kernal che tanto non ci serve,
	e guadagno un po' di ram rendendo accessibile
	la zona $e000/$ffff
*/
		lda #$35
		sta $01

//Init del sid: seleziono la subtune 0 (la prima) e faccio
//una jsr alla routine di inizializzazione
		lda #0
		jsr $1000

//---------------------------------------------------------------
/*	Disabilito i CIA irq e abilito i RASTER irq
	(sembra complicato ma non lo è, comunque
	per il momento puoi ignorare questo blocco)
*/
		lda #$7f
		ldx #$01
		sta $dc0d    
		sta $dd0d 
		stx $d01a    

		lda $dc0d    
		lda $dd0d   
		asl $d019    
//---------------------------------------------------------------
		
		//punto l'irq in fondo al bordo
		lda #<low_irq //lo byte address
		sta $fffe
		lda #>low_irq //hi byte address
		sta $ffff

		lda #$e4 	//l'irq partirà alla linea $e4 (dove inizia il pannello dei punteggi)
		sta $d012

		lda #$1b	//schermo in char mode
		sta $d011

		jsr init_scene //via alla routine di inizializzazione schermo (functions.asm)

		cli 	//via libera agli irq
		jmp * //il "main" non fa niente, lavoriamo tutto in IRQ

//---------------------------------------------------------------

/*
Questo irq viene triggerato una volta per piattaforma (quindi 3 nel nostro caso)
ogni volta viene triggerato 8 linee prima della prox linea di sprite, cosi abbiamo il tempo
di multiplexare tutto con calma
*/
top_irq:
		pha txa pha tya pha //salvo A,X,Y nello stack

		asl $d019 //ack dell'interrupt

		lda platform
		cmp #3
		beq goto_low

		//riposiziono gli sprite
		jsr reposition_sprite
		inc platform

		ldx platform

		lda sprite_y,x
		sec
		sbc #8		//il prox irq sarà 8 linee prima del prox sprite
		sta $d012

		jmp next_irq
goto_low:
		lda #$e4
		sta $d012
		lda #$1b
		sta $d011

		lda #<low_irq //lo byte address
		sta $fffe
		lda #>low_irq //hi byte address
		sta $ffff

next_irq:

.if(DEBUG_IRQ){
		inc $d020
		ldy #30
!:
		dey
		bne !-
		dec $d020
}

		pla tay pla tax pla //recupero A,X,Y dallo stack
		rti
//---------------------------------------------------------------


//---------------------------------------------------------------
//Qui siamo nel bordo inferiore, da linea $fa in giu
low_irq:
		pha txa pha tya pha //salvo A,X,Y nello stack

		lda #BG_COLOUR
		sta $d021

		asl $d019 //ack dell'interrupt

.if(DEBUG_MUSIC){
		inc $d020
}
		jsr $1003 //play della musica
.if(DEBUG_MUSIC){
		dec $d020
}

		//Gestisco il movimento degli sprites aggiornando la tabella delle x
.if(DEBUG_MOVEMENT){
		inc $d020
}
		jsr move_enemies

.if(DEBUG_MOVEMENT){
		dec $d020
}

		lda #0
		sta platform
		sta global_sprite_counter

		lda sprite_y
		sec
		sbc #8		//il prox irq sarà 8 linee prima del primo sprite
		sta $d012

		lda #$1b
		sta $d011

		lda #<top_irq //lo byte address
		sta $fffe
		lda #>top_irq //hi byte address
		sta $ffff

		pla tay pla tax pla //recupero A,X,Y dallo stack
		rti
//---------------------------------------------------------------

//Finto charset
.pc = charset "charset"
.fill $8,0     //0: blocco vuoto
.fill $8,$ff   //1: blocco pieno
.fill $8,$33   //2: blocco mezzo pieno per score board

.pc = platform_map "platform & sprites map / tables"
.import source "data/platforms_sprites_data.asm"

.pc = bonus_sprite_wob_tables "bonus sprite wob tables"
.import source "data/bonus_sprite_wob.asm"

.pc = * "funzioni"
.import source "functions.asm"

//Definizioni sprite
.pc = sprites "sprites definitions"
.fill $40,$ff