//piattaforme: y,x, e lunghezza. si leggono in colonna
//ad es la prima si trova a (4,10) e ha lunghezza 10. occhio che lo (0,0) è 
//il punto IN ALTO A SX dello schermo :)

//Coordinate y delle piattaforme (in termini di righe dello schermo. 1 riga = 8px)
.var plt_y = List().add(4,12,17,21)

//Coordinate x inizio/fine delle piattaforme (in termini di caratteri dello schermo. 1 char = 8px)
.var plt_x_start = List().add(4,2,14,0)
.var plt_x_end = List().add(40,24,40,40)

.align plt_y.size()*3
platforms_y:
.for(var x=0; x<plt_y.size(); x++){
	.byte plt_y.get(x)
}

platforms_x_start:
.for(var x=0; x<plt_y.size(); x++){
	.byte plt_x_start.get(x)
}

platforms_x_end:
.for(var x=0; x<plt_y.size(); x++){
	.byte plt_x_end.get(x)
}

//Bounds in PIXEL (valori a 16 bit) delle piattaforme
.align $80
platforms_bound_left_lo:
.for(var x=0;x<plt_x_start.size();x++){
	.byte <24+plt_x_start.get(x)*8+1
}

platforms_bound_left_hi:
.for(var x=0;x<plt_x_start.size();x++){
	.byte >24+plt_x_start.get(x)*8+1
}

platforms_bound_right_lo:
.for(var x=0;x<plt_x_start.size();x++){
	.byte <plt_x_end.get(x)*8
}

platforms_bound_right_hi:
.for(var x=0;x<plt_x_start.size();x++){
	.byte >plt_x_end.get(x)*8
}


//tabella degli indirizzi delle righe dello schermo.
//ogni riga sono $28 (40) caratteri, per cui ad es la riga 3
//inizia a schermo+3*$28
//chiaramente le tabelle sono a 16 bit perchè lo schermo sono
//$3ff bytes!
.align 50
screen_rows_hi:
.for(var x=0;x<25;x++){
	.byte >screen+[x*$28]
}

screen_rows_lo:
.for(var x=0;x<25;x++){
	.byte <screen+[x*$28]
}

.align $80
.pc = * "sprite y table"
sprite_y:
.for(var y=0;y<plt_y.size();y++){
	.byte plt_y.get(y)*8+29
}

.align $80
.pc = * "sprite x lo table"
sprite_x_lo:
.for(var y=0;y<plt_y.size();y++){
	.for(var x=0;x<6;x++){
		.byte <plt_x_start.get(y)*8+[x]*28 + 24
	}
}

.var d010_val = List().add(
						%00000001,
						%00000010,
						%00000100,
						%00001000,
						%00010000,
						%00100000,
						%01000000,
						%10000000
					)

/*
Posizioni iniziali degli sprites
Tutti gli sprite sono messi in fila
Partendo dalla sx della piattaforma.

Anche qui gli indirizzi sono a 16 bit, se sforo $ff
devo settare il bit corretto su $d010
*/
.var LeftRightList = List()
.align plt_y.size()*6
.pc = * "sprite x hi table"
sprite_x_hi:
.for(var y=0;y<plt_y.size();y++){
	.var d010_byte = 0
	.for(var x=0;x<6;x++){
		.var tmp = >plt_x_start.get(y)*8+[x]*28 + 24
		.if(tmp>0){
			.eval d010_byte = d010_byte | d010_val.get(x+1)
		}
		.eval LeftRightList.add(tmp)
	}
	.byte d010_byte
}


.align plt_y.size()*6
sprite_left_right:
.for(var x=0;x<LeftRightList.size();x++){
	.byte LeftRightList.get(x)
}

.align plt_y.size()*6
sprite_dir:
.fill 48,0